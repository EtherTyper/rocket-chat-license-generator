import crypto from "crypto";
import fs from "fs";

const { publicKey, privateKey } = crypto.generateKeyPairSync("rsa", {
  modulusLength: 4096,
  publicExponent: 65537,
});

console.log(publicKey.asymmetricKeyDetails);
console.log(publicKey.asymmetricKeyType);

const publicPEM = publicKey.export({ format: "pem", type: "spki" });
const publicBase64 = Buffer.from(publicPEM, "utf-8").toString("base64");

const privatePEM = privateKey.export({ format: "pem", type: "pkcs1" });

fs.writeFileSync("publicBase64.txt", publicBase64);
fs.writeFileSync("privateKey.asc", privatePEM);

console.log(publicBase64);
console.log(privatePEM);

console.log(
  crypto.createPublicKey(privateKey).export({ format: "pem", type: "spki" }) ===
    publicPEM
);
