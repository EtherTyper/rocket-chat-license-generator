import crypto from "crypto";
import fs from "fs";

const privateKey = crypto.createPrivateKey(fs.readFileSync("privateKey.asc"));
const publicKey = crypto.createPublicKey(privateKey);

console.log(privateKey.asymmetricKeyDetails);
console.log(privateKey.asymmetricKeyType);

const licenseObject = {
  tag: {
    name: "Go Longhorns",
    color: "#bf5700",
  },
  modules: ["*"],
};

const licenseString = JSON.stringify(licenseObject);
console.log(licenseString);

const encryptedLicense = crypto
  .privateEncrypt(privateKey, Buffer.from(licenseString, "utf-8"))
  .toString("base64");
console.log(encryptedLicense);

fs.writeFileSync("encryptedLicense.txt", encryptedLicense);

console.log(
  crypto
    .publicDecrypt(publicKey, Buffer.from(encryptedLicense, "base64"))
    .toString("utf-8") === licenseString
);
