To create and start containers:
```bash
podman network create rocket-chat-network
podman run --net=rocket-chat-network --name db -d mongo:5.0.9 --replSet rs0 --oplogSize 128
podman exec -ti db mongo --eval "printjson(rs.initiate())"
podman run --net=rocket-chat-network -p 80:3000 --name rocketchat --env MONGO_OPLOG_URL=mongodb://db:27017/local -d rocket.chat
```

To back up database: Make sure `rocketchat` is stopped and `db` is running.

```bash
podman exec db mongodump --archive > ~/Documents/rocket-chat/db.dump
```

To recreate and restore database: Make sure `rocketchat` is stopped and remove `db`.

```bash
podman run --net=rocket-chat-network --name db -d mongo:5.0.9 --replSet rs0 --oplogSize 128
podman exec -ti db mongo --eval "printjson(rs.initiate())"
podman exec -i db mongorestore --archive < ~/Documents/rocket-chat/db.dump
```

To replace license signing key: Make sure `rocketchat` is running.
```bash
podman exec -u 0 -it rocketchat sed -i 's/LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQ0lqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FnOEFNSUlDQ2dLQ0FnRUFxV1Nza2Q5LzZ6Ung4a3lQY2ljcwpiMzJ3Mnd4VnV3N3lCVDk2clEvOEQreU1lQ01POXdTU3BIYS85bkZ5d293RXRpZ3B0L3dyb1BOK1ZHU3didHdQCkZYQmVxRWxCbmRHRkFsODZlNStFbGlIOEt6L2hHbkNtSk5tWHB4RUsyUkUwM1g0SXhzWVg3RERCN010eC9pcXMKY2pCL091dlNCa2ppU2xlUzdibE5JVC9kQTdLNC9DSjNvaXUwMmJMNEV4Y2xDSGVwenFOTWVQM3dVWmdweE9uZgpOT3VkOElYWUs3M3pTY3VFOEUxNTdZd3B6Q0twVmFIWDdaSmY4UXVOc09PNVcvYUlqS2wzTDYyNjkrZUlPRXJHCndPTm1hSG56Zmc5RkxwSmh6Z3BPMzhhVm43NnZENUtLakJhaldza1krNGEyZ1NRbUtOZUZxYXFPb3p5RUZNMGUKY0ZXWlZWWjNMZWg0dkVNb1lWUHlJeng5Nng4ZjIveW1QbmhJdXZRdjV3TjRmeWVwYTdFWTVVQ2NwNzF6OGtmUAo0RmNVelBBMElEV3lNaWhYUi9HNlhnUVFaNEdiL3FCQmh2cnZpSkNGemZZRGNKZ0w3RmVnRllIUDNQR0wwN1FnCnZMZXZNSytpUVpQcnhyYnh5U3FkUE9rZ3VyS2pWclhUVXI0QTlUZ2lMeUlYNVVsSnEzRS9SVjdtZk9xWm5MVGEKU0NWWEhCaHVQbG5DR1pSMDFUb1RDZktoTUcxdTBDRm5MMisxNWhDOWZxT21XdjlRa2U0M3FsSjBQZ0YzVkovWAp1eC9tVHBuazlnbmJHOUpIK21mSDM5Um9GdlROaW5Zd1NNdll6dXRWT242OXNPemR3aERsYTkwbDNBQ2g0eENWCks3Sk9YK3VIa29OdTNnMmlWeGlaVU0wQ0F3RUFBUT09Ci0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLQo=/LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQ0lqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FnOEFNSUlDQ2dLQ0FnRUExdnZCVnBMckVoZ2VGMi9LZmNCKwpTZHNEb1FoSk1zc1JKUDR0RGFvOFI4U0ZBZVhrWWdTcXl3a3pjck5Pdm43ejlTOVgxQ01qNDk2S1E2WEUwa1lPCkZKaTAybFF6alkrdjlWQkRQKzFiRE04K2hmZ0sxZmVyc25rcEJ5MnhWZ00vMCsrZEtNNlRCaWFGVmJZUTREN0cKcjBQUFhPdy9ZcXpYQ01BQ1FTRzNCSEZQMnkzOVNmN3E5eFpuRHNkeEdMTkFHWERaZWVDbmZPQkdSb0NnOGNVRQpweVFqaWtmVjg5OTYwOGhhT0liUFdJeWQyaHJ5OXJOSXRkWG9vRDFxKzhJemREWjZJRDh0dnFFL05IakZTN1oyClNFOHJJQUxVOUVzSnVOQVR6OWp0TW94ZUFoUGZ6YVhjK3lOK3pWdTFEdlYvbUFVZWVhdXJUQTdqdnEzTWVEK0YKWDNic1ZZcStaUmJ4ZXNRc01rQ0ErZmR3VEp3bVVhM3hTQm5na0h3cmxLQVFvVUJBQUxQVUJXU0U3M0lZc2JPMwpsTm0zSURHbzV4OFBnNnNGNjhjYU5YLytKS29PcTdWbHVGOFVrOGZucjV5Q1o0RWlkb0h6OG5EMi9CNCtpdkVLCnJFd3dMN0VHeHBucVlETmovU0tocHBWakFPYS93ZytxaUJESmNMK2pITTJVMDRmMiticHg3dlpxUExPa0R6RlYKYnUvTGZZRGNrQ3dwV3Fqem12aGRyUk45czZzWldhOFk4aS9yNUxSQmpSM2JoUDhQc1hIc3l4SFVoWTJtb0xZZQpDZzZCdXY5SzQ2dmdoS2k5cjA4d2FuTWNvWHBlaitRamFOZ1VZeDhvU0lTTkRtczRlSy9aQ080L2IvTkpDWGFnCi83bmhXcFdzeW9DUW9oZEJXVmw1TDkwQ0F3RUFBUT09Ci0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLQo=/g' /app/bundle/programs/server/app/app.js
podman restart rocketchat
```

Remove passwords from DB before publishing: Make sure `rocketchat` is stopped and `db` is running, and launch `podman exec -it db mongosh`.
```bash
use meteor
db.users.updateMany({}, {$unset: {"services.password": ""}})
```
